<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
namespace App\Constant;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 * @method getMessage(int $statusCode, array $statusCodeParams = [])
 */
class StatusCode extends AbstractConstants
{
    /**
     * @Message("messages.SUCCESS")
     */
    const SUCCESS = 0;

    /**
     * @Message("messages.ERROR_NOT_EXISTS_USER")
     */
    const ERROR_NOT_EXISTS_USER = 20001;
}
