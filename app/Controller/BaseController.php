<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
namespace App\Controller;

class BaseController extends AbstractController
{
    protected function successJson($data)
    {
        return $this->response->json($data);
    }
}
