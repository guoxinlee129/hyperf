<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
namespace App\Controller;

use App\Constant\StatusCode;
use App\Exception\BadGatewayException;
use App\Exception\BadRequestException;
use App\Exception\ForbiddenException;
use App\Exception\GatewayTimeoutException;
use App\Exception\InternalServerErrorException;
use App\Exception\MovedPermanentlyException;
use App\Exception\NotFoundException;
use App\Exception\RequestTimeoutException;
use App\Exception\RuntimeException;
use App\Kernel\Response;
use App\Service\Wechat\MiniProgramService;
use Hyperf\HttpMessage\Exception\HttpException;
use Hyperf\HttpServer\Annotation\AutoController;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface;

/**
 * @AutoController
 * Class Exception
 */
class ExceptionController
{
    public function exceptionTest(RequestInterface $request, ResponseInterface $response)
    {
        $app = new MiniProgramService();
        // throw new UnsupportedMediaTypeHttpException();
        // throw new UnprocessableEntityHttpException();
        // throw new UnauthorizedHttpException();
        // throw new ServerErrorHttpException();
        // throw new RangeNotSatisfiableHttpException();
        // throw new NotFoundHttpException();
        // throw new NotAcceptableHttpException();
        // throw new MethodNotAllowedHttpException();
        // throw new ForbiddenHttpException();
        // throw new BadRequestHttpException();
        throw new HttpException(200);
    }

    public function badGatewayException()
    {
        throw new BadGatewayException(Response::getResult(StatusCode::ERROR_NOT_EXISTS_USER, [], ['a']));
    }

    public function badRequestException()
    {
        throw new BadRequestException();
    }

    public function forbiddenException()
    {
        throw new ForbiddenException();
    }

    public function gatewayTimeoutException()
    {
        throw new GatewayTimeoutException();
    }

    public function internalServerErrorException()
    {
        throw new InternalServerErrorException();
    }

    public function movedPermanentlyException()
    {
        throw new MovedPermanentlyException();
    }

    public function notFoundException()
    {
        throw new NotFoundException();
    }

    public function requestTimeoutException()
    {
        throw new RequestTimeoutException();
    }

    public function runtimeException()
    {
        throw new RuntimeException();
    }

    public function unauthorizedException()
    {
        throw new UnauthorizedException();
    }

    public function exception()
    {
        throw new \Exception('这是Exception');
    }
}
