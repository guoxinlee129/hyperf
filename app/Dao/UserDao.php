<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
namespace App\Dao;

use App\Constant\StatusCode;
use App\Exception\NotFoundException;
use App\Kernel\Response;
use App\Model\BalletmeUser;
use Hyperf\HttpMessage\Exception\NotFoundHttpException;

class UserDao
{
    public function find(int $id)
    {
        // $loader = ApplicationContext::getContainer()->get(EagerLoader::class);
        // $loader->load($user, ['integrals']);
        // if ($user) {
        //     return json_decode($user, true);
        // }
        // var_dump($user);
        // return null;
        $userModel = BalletmeUser::find($id);
        if (! $userModel) {
            throw new NotFoundException(Response::getResult(StatusCode::ERROR_NOT_EXISTS_USER, [], ['dsadasd', 1323]));
        }
        return $userModel->toArray();
    }

    public function findFromCache(int $id)
    {
        $userModel = BalletmeUser::findFromCache($id);
        if (! $userModel) {
            throw new NotFoundException();
        }
        return $userModel->toArray();
    }

    public function findMany(array $ids)
    {
        $userModel = BalletmeUser::findMany($ids);
        if (! $userModel) {
            throw new NotFoundHttpException();
        }
        return $userModel->toArray();
    }

    public function findManyFromCache(array $ids): array
    {
        $userModel = BalletmeUser::findManyFromCache([]);
        if (! $userModel) {
            throw new NotFoundHttpException();
        }
        return $userModel->toArray();
    }
}
