<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
namespace App\Event;

use App\Model\BalletmeUser;

class UserRegistered
{
    // 建议这里定义成 public 属性，以便监听器对该属性的直接使用，或者你提供该属性的 Getter
    public object $user;

    public function __construct(BalletmeUser $user)
    {
        $this->user = $user;
    }
}
