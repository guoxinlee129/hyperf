<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
namespace App\Exception;

class BadGatewayException extends RuntimeException
{
    protected $httpCode = 502;

    public function __construct(array $params = [])
    {
        parent::__construct($params);
    }
}
