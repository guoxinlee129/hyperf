<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
namespace App\Exception;

class BadRequestException extends RuntimeException
{
    protected $httpCode = 400;

    public function __construct(array $params = [])
    {
        parent::__construct($params);
    }
}
