<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
namespace App\Exception;

use Hyperf\ExceptionHandler\ExceptionHandler;
use Hyperf\HttpMessage\Exception\BadRequestHttpException;
use Hyperf\HttpMessage\Exception\ForbiddenHttpException;
use Hyperf\HttpMessage\Exception\HttpException;
use Hyperf\HttpMessage\Exception\MethodNotAllowedHttpException;
use Hyperf\HttpMessage\Exception\NotAcceptableHttpException;
use Hyperf\HttpMessage\Exception\NotFoundHttpException;
use Hyperf\HttpMessage\Exception\RangeNotSatisfiableHttpException;
use Hyperf\HttpMessage\Exception\ServerErrorHttpException;
use Hyperf\HttpMessage\Exception\UnauthorizedHttpException;
use Hyperf\HttpMessage\Exception\UnprocessableEntityHttpException;
use Hyperf\HttpMessage\Exception\UnsupportedMediaTypeHttpException;
use Hyperf\Validation\ValidationException;
use Psr\Http\Message\ResponseInterface;
use Throwable;

class HyperfHandler extends ExceptionHandler
{
    public function handle(Throwable $throwable, ResponseInterface $response)
    {
        var_dump('---------------' . PHP_EOL);
        switch (true) {
            # 下方是框架内自带的Exception
            case $throwable instanceof ValidationException:
                #415 不支持的媒体类型
                throw new BadRequestException([
                    'msg' => $throwable->getMessage(),
                ]);
            case $throwable instanceof UnsupportedMediaTypeHttpException:
                #415 不支持的媒体类型
                throw new BadRequestException([
                    'msg' => __('message.UnsupportedMediaType'),
                ]);
            case $throwable instanceof UnprocessableEntityHttpException:
                #422 不可处理实体
                throw new BadRequestException([
                    'msg' => __('message.UnprocessableEntity'),
                ]);
            case $throwable instanceof UnauthorizedHttpException:
                #401 未授权
                throw new UnauthorizedException([
                    'msg' => __('message.UnprocessableEntity'),
                ]);
            case $throwable instanceof ServerErrorHttpException:
                #500 服务器错误
                throw new \Exception($throwable->getMessage());
            case $throwable instanceof RangeNotSatisfiableHttpException:
                #416 范围不可满足
                throw new UnauthorizedException([
                    'msg' => __('message.RangeNotSatisfiableHttpException'),
                ]);
            case $throwable instanceof NotFoundHttpException:
                #404
                throw new NotFoundException([
                    'msg' => __('message.NotFoundHttpException'),
                ]);
            case $throwable instanceof NotAcceptableHttpException:
                #406 请求不可接受
                throw new BadRequestException([
                    'msg' => __('message.NotAcceptableHttpException'),
                ]);
            case $throwable instanceof MethodNotAllowedHttpException:
                #405 无权访问该方法
                throw new BadRequestException([
                    'msg' => __('message.MethodNotAllowedHttpException'),
                ]);
            case $throwable instanceof ForbiddenHttpException:
                #403
                throw new ForbiddenException([
                    'msg' => __('message.ForbiddenHttpException'),
                ]);
            case $throwable instanceof BadRequestHttpException:
                #400
                throw new ForbiddenException([
                    'msg' => __('message.BadRequestHttpException'),
                ]);
            case $throwable instanceof HttpException:
                #400
                throw new BadRequestException();
            default:
        }
        return $response;
    }

    public function isValid(Throwable $throwable): bool
    {
        return false;
    }
}
