<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
namespace App\Exception;

class InternalServerErrorException extends RuntimeException
{
    protected $httpCode = 500;

    public function __construct(array $params = [])
    {
        parent::__construct($params);
    }
}
