<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
namespace App\Exception;

class MovedPermanentlyException extends RuntimeException
{
    protected $httpCode = 301;

    public function __construct(array $params = [])
    {
        parent::__construct($params);
    }
}
