<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
namespace App\Exception;

class NotFoundException extends RuntimeException
{
    protected $httpCode = 404;

    public function __construct(array $params = [])
    {
        parent::__construct($params);
    }
}
