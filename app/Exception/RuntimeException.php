<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
namespace App\Exception;

class RuntimeException extends \RuntimeException
{
    protected $code = -1;

    public function __construct(array $params = [])
    {
        parent::__construct();
        if (array_key_exists('code', $params)) {
            $this->code = $params['code'];
        }
        if (array_key_exists('msg', $params)) {
            $this->message = __($params['msg']);
        } else {
            $array = explode('\\', get_called_class());
            $this->message = __('messages.' . array_pop($array));
        }
        if (array_key_exists('data', $params)) {
            $this->data = $params['data'];
        }
    }

    public function getHttpCode(): int
    {
        return $this->httpCode ?? 500;
    }
}
