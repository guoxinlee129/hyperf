<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
use Hyperf\Utils\ApplicationContext;

//
// if (!function_exists('di')) {
//     function di($id = null)
//     {
//         $container = ApplicationContext::getContainer();
//         if ($id) {
//             return $container->get($id);
//         }
//         return $container;
//     }
// }
/*
 * 正常情况：多语言不传参数
 * 异常情况：可能会传递参数
 */
if (! function_exists('filter')) {
    function filter(&$array)
    {
        // if ($array === []) {
        //     return new stdClass();
        // }
        foreach ($array as &$value) {
            if (is_array($value)) {
                $value = filter($value);
            }
            // if ($value === []) {
            //     $value = new stdClass();
            // }
            if ($value === null) {
                $value = '';
            }
        }
        return $array;
    }
}
