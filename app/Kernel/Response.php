<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
namespace App\Kernel;

use App\Constant\StatusCode;

class Response
{
    public static function getResult(int $code, array $params = [], $data = []): array
    {
        $msg = self::getMsg($code, $params);
        return [
            'msg' => $msg,
            'code' => $code,
            'data' => $data,
        ];
    }

    public static function getMsg($code, $params): string
    {
        return StatusCode::getMessage($code, $params);
    }
}
