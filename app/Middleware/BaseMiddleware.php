<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
namespace App\Middleware;

use Hyperf\Contract\TranslatorInterface;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Context;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class BaseMiddleware implements MiddlewareInterface
{
    protected ContainerInterface $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $params = $request->getQueryParams();

        # 语言匹配
        $container = ApplicationContext::getContainer();
        $translator = $container->get(TranslatorInterface::class);
        $currentLang = $params['lang'] ?? 'zh_CN';
        $allowLang = config('allow_lang', []);
        if (in_array(
            $currentLang,
            $allowLang,
            true
        )) {
            $translator->setLocale($currentLang);
        }

        # 为每一个请求增加一个qid
        $request = Context::override(ServerRequestInterface::class, function (ServerRequestInterface $request) {
            $id = $this->getRequestId();
            var_dump($id);
            return $request->withAddedHeader('qid', $id);
        });

        # 利用协程上下文存储请求开始的时间，用来计算程序执行时间
        Context::set('request_start_time', microtime(true));

        # http请求标志
        $response = $handler->handle($request);
        $executionMicroTime = bcsub((string) microtime(true), (string) Context::get('request_start_time'), 20);
        $executionSecond = bcdiv((string) $executionMicroTime, '1000000', 20);
        $response = $response->withAddedHeader('Execution-Second', $executionSecond);
        $response = $response->withAddedHeader('Server-Language', $translator->getLocale());
        $response = $response->withoutHeader('Server');
        $response = $response->withAddedHeader('Server', config('app_name'));
        return $response->withAddedHeader('Request-Type', 'http');
    }

    public function getServerLocalIp(): string
    {
        $ip = '127.0.0.1';
        $ips = array_values(swoole_get_local_ip());
        foreach ($ips as $v) {
            if ($v && $v != $ip) {
                $ip = $v;
                break;
            }
        }

        return $ip;
    }

    private function getRequestId()
    {
        // $tmp = $this->request->getServerParams();
        $tmp = ['remote_addr' => '127.0.0.1'];
        // var_dump($tmp);
        $name = strtoupper(substr(md5(gethostname()), 12, 8));
        $remote = strtoupper(substr(md5($tmp['remote_addr']), 12, 8));
        $ip = strtoupper(substr(md5($this->getServerLocalIp()), 14, 4));
        return uniqid() . '-' . $remote . '-' . $ip . '-' . $name;
    }
}
