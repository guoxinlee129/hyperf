<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
namespace App\Model;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property string $unionid
 * @property string $openid
 * @property string $name
 * @property string $avatarUrl
 * @property string $phone
 * @property int $status
 * @property string $birthday
 * @property int $difficulty
 * @property int $identity
 * @property int $is_teacher
 * @property int $teacher_id
 * @property string $city
 * @property string $province
 * @property string $country
 * @property int $is_inside
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $avatar
 * @property int $type
 * @property int $order_total
 * @property int $start_semester_id
 * @property string $real_name
 * @property int $gender
 * @property string $deleted_at
 */
class BalletmeUser extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'balletme_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer', 'difficulty' => 'integer', 'identity' => 'integer', 'is_teacher' => 'integer', 'teacher_id' => 'integer', 'is_inside' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime', 'type' => 'integer', 'order_total' => 'integer', 'start_semester_id' => 'integer', 'gender' => 'integer', 'test' => 'integer'];

    /**
     * 关联用户积分.
     */
    public function integrals()
    {
        return $this->hasOne(BalletmeUserIntegral::class, 'user_id', 'id');
    }
}
