<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
namespace App\Service;

use App\Event\UserRegistered;
use App\Model\BalletmeUser;
use Hyperf\Di\Annotation\Inject;
use Psr\EventDispatcher\EventDispatcherInterface;

class UserService
{
    /**
     * @Inject
     */
    private EventDispatcherInterface $eventDispatcher;

    public function register($request)
    {
        $user = new BalletmeUser();
        $this->eventDispatcher->dispatch(new UserRegistered($user));
        return ['id' => 1];
    }
}
