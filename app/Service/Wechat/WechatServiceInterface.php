<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
namespace App\Service\Wechat;

interface WechatServiceInterface
{
    public function __construct();
}
