<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
return [
    Hyperf\ModelCache\Listener\EagerLoadListener::class,
    Hyperf\DbConnection\Listener\InitTableCollectorListener::class,
];
