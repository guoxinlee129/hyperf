<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
return [
    'http' => [
        App\Middleware\BaseMiddleware::class,
        App\Middleware\AuthMiddleware::class,
        Hyperf\Validation\Middleware\ValidationMiddleware::class,
    ],
];
