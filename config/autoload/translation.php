<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
return [
    'locale' => 'zh_CN',
    'fallback_locale' => 'en',
    'path' => BASE_PATH . '/storage/languages',
];
