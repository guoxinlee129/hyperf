<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
return [
    # Base
    'WELCOME' => 'welcome!',
    'SUCCESS' => 'success!',

    # Hyperf Exception
    'RuntimeException' => 'Failed!',
    'BadRequestHttpException' => 'Bad request!',
    'HttpException' => 'Bad request!',
    'ForbiddenHttpException' => 'Forbidden!',
    'MethodNotAllowedHttpException' => 'Method not allowed!',
    'NotAcceptableHttpException' => 'Not acceptable!',
    'NotFoundHttpException' => 'Not found!',
    'RangeNotSatisfiableHttpException' => 'Range not satisfiable!',
    'ServerErrorHttpException' => 'Network busy!',
    'UnauthorizedHttpException' => 'Unauthorized!',
    'UnprocessableEntityHttpException' => 'Unprocessable entity!',
    'UnsupportedMediaTypeHttpException' => 'Unsupported media type!',
    'ServerException' => 'Network busy!',

    # My System Exception
    'BadGatewayException' => 'Bad Gateway Error!',
    'BadRequestException' => 'Bad Request Error!',
    'ForbiddenException' => 'Forbidden Error!',
    'GatewayTimeoutException' => 'Gateway Timeout Error!',
    'InternalServerErrorException' => 'Internal Server Error!',
    'MovedPermanentlyException' => 'Moved Permanently Error!',
    'NotFoundException' => 'Not Found Error!',
    'RequestTimeoutException' => 'Request Timeout Error!',
    'UnauthorizedException' => 'Unauthorized Error!',

    # 业务
    'ERROR_NOT_EXISTS_USER' => 'User Not Exists!',
];
