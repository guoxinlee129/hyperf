<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
return [
    # Base
    'WELCOME' => '欢迎!',
    'SUCCESS' => '操作成功!',

    # Hyperf Exception
    'RuntimeException' => '操作失败!',
    'BadRequestHttpException' => '请求失败!',
    'HttpException' => '请求失败!',
    'ForbiddenHttpException' => '无权访问!',
    'MethodNotAllowedHttpException' => '无权访问该方法!',
    'NotAcceptableHttpException' => '已拒绝接受本数据!',
    'NotFoundHttpException' => '资源不存在!',
    'RangeNotSatisfiableHttpException' => '范围不允许!',
    'ServerErrorHttpException' => '网络繁忙!',
    'UnauthorizedHttpException' => '未经授权，无法操作!',
    'UnprocessableEntityHttpException' => '无法处理该数据!',
    'UnsupportedMediaTypeHttpException' => '不支持的媒体类型!',
    'ServerException' => '网络繁忙!',

    # My System Exception
    'BadGatewayException' => '网关错误!',
    'BadRequestException' => '参数错误!',
    'ForbiddenException' => '无权访问!',
    'GatewayTimeoutException' => '网络超时!',
    'InternalServerErrorException' => '网络错误!',
    'MovedPermanentlyException' => '网址已永久移动!',
    'NotFoundException' => '资源不存在!',
    'RequestTimeoutException' => '请求超时错误!',
    'UnauthorizedException' => '操作未经授权!',

    # 业务
    'ERROR_NOT_EXISTS_USER' => '用户不存在!',
];
