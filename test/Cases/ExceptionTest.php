<?php

declare(strict_types=1);
/**
 * @author liguoxin
 * @email guoxinlee129@gmail.com
 */
namespace HyperfTest\Cases;

use Hyperf\Testing\Client;
use HyperfTest\HttpTestCase;

/**
 * @internal
 * @coversNothing
 */
class ExceptionTest extends HttpTestCase
{
    /**
     * @var Client
     */
    protected $client;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->client = make(Client::class);
    }

    public function testBadGatewayException()
    {
        $res = $this->client->get('/exception/badGatewayException');
        $this->assertSame(20001, $res['code']);
        $this->assertSame(__('messages.ERROR_NOT_EXISTS_USER'), $res['msg']);
        // $this->assertSame(__('messages.BadGatewayException'), $res['msg']);
        $this->assertSame(['a'], $res['data']);

        $res = $this->client->get('/exception/badGatewayException?lang=en');
        $this->assertSame(20001, $res['code']);
        $this->assertSame(__('messages.ERROR_NOT_EXISTS_USER'), $res['msg']);
        // $this->assertSame(__('messages.BadGatewayException'), $res['msg']);
        $this->assertSame(['a'], $res['data']);
    }

    public function testBadRequestException()
    {
        $res = $this->client->get('/exception/badRequestException');
        $this->assertSame(-1, $res['code']);
        $this->assertSame(__('messages.BadRequestException'), $res['msg']);
        $this->assertSame([], $res['data']);

        $res = $this->client->get('/exception/badRequestException?lang=en');
        $this->assertSame(-1, $res['code']);
        $this->assertSame(__('messages.BadRequestException'), $res['msg']);
        $this->assertSame([], $res['data']);
    }

    public function testForbiddenException()
    {
        $res = $this->client->get('/exception/forbiddenException');
        $this->assertSame(-1, $res['code']);
        $this->assertSame(__('messages.ForbiddenException'), $res['msg']);
        $this->assertSame([], $res['data']);

        $res = $this->client->get('/exception/forbiddenException?lang=en');
        $this->assertSame(-1, $res['code']);
        $this->assertSame(__('messages.ForbiddenException'), $res['msg']);
        $this->assertSame([], $res['data']);
    }

    public function testGatewayTimeoutException()
    {
        $res = $this->client->get('/exception/gatewayTimeoutException');
        $this->assertSame(-1, $res['code']);
        $this->assertSame(__('messages.GatewayTimeoutException'), $res['msg']);
        $this->assertSame([], $res['data']);

        $res = $this->client->get('/exception/gatewayTimeoutException?lang=en');
        $this->assertSame(-1, $res['code']);
        $this->assertSame(__('messages.GatewayTimeoutException'), $res['msg']);
        $this->assertSame([], $res['data']);
    }

    public function testInternalServerErrorException()
    {
        $res = $this->client->get('/exception/internalServerErrorException');
        $this->assertSame(-1, $res['code']);
        $this->assertSame(__('messages.InternalServerErrorException'), $res['msg']);
        $this->assertSame([], $res['data']);

        $res = $this->client->get('/exception/internalServerErrorException?lang=en');
        $this->assertSame(-1, $res['code']);
        $this->assertSame(__('messages.InternalServerErrorException'), $res['msg']);
        $this->assertSame([], $res['data']);
    }

    public function testMovedPermanentlyException()
    {
        $res = $this->client->get('/exception/movedPermanentlyException');
        $this->assertSame(-1, $res['code']);
        $this->assertSame(__('messages.MovedPermanentlyException'), $res['msg']);
        $this->assertSame([], $res['data']);

        $res = $this->client->get('/exception/movedPermanentlyException?lang=en');
        $this->assertSame(-1, $res['code']);
        $this->assertSame(__('messages.MovedPermanentlyException'), $res['msg']);
        $this->assertSame([], $res['data']);
    }

    public function testNotFoundException()
    {
        $res = $this->client->get('/exception/notFoundException');
        $this->assertSame(-1, $res['code']);
        $this->assertSame(__('messages.NotFoundException'), $res['msg']);
        $this->assertSame([], $res['data']);

        $res = $this->client->get('/exception/notFoundException?lang=en');
        $this->assertSame(-1, $res['code']);
        $this->assertSame(__('messages.NotFoundException'), $res['msg']);
        $this->assertSame([], $res['data']);
    }

    public function testRequestTimeoutException()
    {
        $res = $this->client->get('/exception/requestTimeoutException');
        $this->assertSame(-1, $res['code']);
        $this->assertSame(__('messages.RequestTimeoutException'), $res['msg']);
        $this->assertSame([], $res['data']);

        $res = $this->client->get('/exception/requestTimeoutException?lang=en');
        $this->assertSame(-1, $res['code']);
        $this->assertSame(__('messages.RequestTimeoutException'), $res['msg']);
        $this->assertSame([], $res['data']);
    }

    public function testRuntimeException()
    {
        $res = $this->client->get('/exception/runtimeException');
        $this->assertSame(-1, $res['code']);
        $this->assertSame(__('messages.RuntimeException'), $res['msg']);
        $this->assertSame([], $res['data']);

        $res = $this->client->get('/exception/runtimeException?lang=en');
        $this->assertSame(-1, $res['code']);
        $this->assertSame(__('messages.RuntimeException'), $res['msg']);
        $this->assertSame([], $res['data']);
    }

    public function testUnauthorizedException()
    {
        $res = $this->client->get('/exception/unauthorizedException');
        $this->assertSame(-1, $res['code']);
        $this->assertSame(__('messages.UnauthorizedException'), $res['msg']);
        $this->assertSame([], $res['data']);

        $res = $this->client->get('/exception/unauthorizedException?lang=en');
        $this->assertSame(-1, $res['code']);
        $this->assertSame(__('messages.UnauthorizedException'), $res['msg']);
        $this->assertSame([], $res['data']);
    }

    public function testException()
    {
        $res = $this->client->get('/exception/exception');
        $this->assertSame(-1, $res['code']);
        $this->assertSame('这是Exception', $res['msg']);
        $this->assertSame([], $res['data']);

        $res = $this->client->get('/exception/exception?lang=en');
        $this->assertSame(-1, $res['code']);
        $this->assertSame('这是Exception', $res['msg']);
        $this->assertSame([], $res['data']);
    }
}
